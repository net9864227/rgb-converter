﻿using System;

#pragma warning disable

namespace RgbConverter
{
    public static class Rgb
    {
        /// <summary>
        /// Gets hexadecimal representation source RGB decimal values.
        /// </summary>
        /// <param name="red">The valid decimal value for RGB is in the range 0-255.</param>
        /// <param name="green">The valid decimal value for RGB is in the range 0-255.</param>
        /// <param name="blue">The valid decimal value for RGB is in the range 0-255.</param>
        /// <returns>Returns hexadecimal representation source RGB decimal values.</returns>
        public static string GetHexRepresentation(int red, int green, int blue)
        {
            if (red < 0) red = 0;
            if (blue < 0) blue = 0;
            if (green < 0) green = 0;
            if (red > 255) red = 255;
            if (blue > 255) blue = 255;
            if (green > 255) green = 255;

            string r =Convert.ToString(red, 16);
            string g =Convert.ToString(green, 16);
            string b =Convert.ToString(blue, 16);

            r = r.ToUpper();
            g = g.ToUpper();
            b = b.ToUpper();

            if (r.Length == 1) r = "0" + r;
            if (g.Length == 1) g = "0" + g;
            if (b.Length == 1) b = "0" + b;

            return r + g + b;

        }
    }
}
